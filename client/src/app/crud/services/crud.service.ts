import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CrudUrl} from '../crud-url';
import {Subject} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  private items = new Subject<any[]>();

  items$ = this.items.asObservable();

  totalItems = 0;

  crudUrl: CrudUrl;

  constructor(private http: HttpClient) {
  }

  list(queryParams = {}) {
    const url = this.getFullUrl(queryParams);
    return this.http.get(url, {observe: 'response'})
      .toPromise().then(response => {
        const totalItems = response.headers.get('X-Total-Count') || '0';
        this.totalItems = parseInt(totalItems, 10);
        this.items.next(response.body as any[]);
      });
  }

  getFullUrl(queryParams = {}) {
    let base = environment.endpoint;
    base = base.endsWith('/') ? base : `${base}/`;
    queryParams = Object.assign({
      page: this.crudUrl.page,
      limit: this.crudUrl.limit
    }, queryParams);
    const query = [];
    for (const param in queryParams) {
      if (!queryParams.hasOwnProperty(param)) {
        continue;
      }
      query.push(`${param}=${queryParams[param]}`);
    }
    return `${base}${this.crudUrl.url}` + (query.length ? `?${query.join('&')}` : '');
  }
}
