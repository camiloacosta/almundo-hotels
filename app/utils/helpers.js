const path = require('path');
const HttpError = require('../exceptions/http-error');

const IMAGE_EXTENSION_IS_NOT_VALID = 'Image extension isn\'t valid';

const ENV_DEVELOPMENT = 'development';
const ENV_PRODUCTION = 'production';

const getEnviroment = module.exports.getEnviroment = () => process.env.ENVIROMENT || ENV_DEVELOPMENT;

module.exports.isDevelopment = () => getEnviroment() === ENV_DEVELOPMENT;

module.exports.isProduction = () => getEnviroment() === ENV_PRODUCTION;

module.exports.normalizeJoiError = joiError => joiError.details
    .map(detail => ({id: detail.path[0], message: detail.message}))
    .reduce((obj, item) => {
        obj[item.id] = item.message;
        return obj;
    }, {});

module.exports.checkImageExtensionMulter = (req, file, callback) => {
    const ext = path.extname(file.originalname);
    const allowedExt = process.env.IMAGE_EXTENSIONS || '';
    if (allowedExt.split(',').indexOf(ext) < 0) {
        return callback(new HttpError(400, IMAGE_EXTENSION_IS_NOT_VALID));
    }
    return callback(null, true);
};