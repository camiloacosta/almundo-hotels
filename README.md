# Almundo Examen Front-end

## Requiere
- MongoDB

## Primeros pasos
- Hacer la instalación de los paquetes de Node en el servidor y en el cliente
- Se debe crear el archivo de variables de entorno (`.env.dev` o `.env.prod`) en el directorio `enviroments`.
- Tambien modificar los valores de los archivos `enviroment` de angular que están en el directorio `client/src/enviroments`.
- En el directorio `migration` se ecuentra el archivo comprimido `hotels.zip`, extraer su contenido al directorio `images`.
- Correr el comando `npm run migrate:dev` si se configuró la variable de entorno `.env.dev` o `npm run migrate:prod` si se configuró la variable de entorno `.env.prod`.

### Configuración de entorno
```
ENVIROMENT = Puede tomar los valores `dev`, `development`, `prod` o `production`
PORT = Puerto en el que correrá la aplicación
CONNECTION_STRING = Cadena de conexión de MongoDB
CORS_WHITELIST = lista separada por comas de los clientes permitidos
IMAGE_EXTENSIONS = lista separada por comas de las extensiones que admite al subir una image (incluir el punto) `.png,.jpg,.jpeg`
BASE_SERVER = dirección donde corre la aplicación
```

## Desarrollo
Para ejecutar en modo desarrollo se debe correr el comando `npm run start:dev`

## Producción
Para ejecutar en modo producción se debe ejecutar el comando `npm run client:build` seguido de `npm start`