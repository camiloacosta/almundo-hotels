import {Component} from '@angular/core';
import {ListController} from '../../../crud/controllers/list-controller';
import {HotelService} from '../hotel.service';
import {ExpandAnimation} from '../shared/animations/expand';

@Component({
  selector: 'alm-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.scss', '../shared/styles/expandable.scss'],
  animations: [
    ExpandAnimation('expand')
  ]
})
export class HotelListComponent extends ListController {

  constructor(crudManager: HotelService) {
    super(crudManager);
  }

  setFilter(filter: { name: string, stars: string }) {
    super.setFilter(filter);
  }

}
