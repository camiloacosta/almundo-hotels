const path = require('path');
const joi = require('joi');
const multer = require('multer');
const HotelDto = require('../models/dto/hotel.dto');
const helpers = require('../utils/helpers');
const httpHelpers = require('../utils/http');
const HotelRepository = require('../repositories/hotel.repository');
const {X_TOTAL_COUNT_HEADER_NAME} = require('../utils/constants');

const storage = name => multer.diskStorage({
    destination: path.join(process.cwd(), 'images'),
    filename: (req, file, cb) => {
        return cb(null, name + path.extname(file.originalname));
    }
});

const upload = name => multer({
    storage: storage(name),
    fileFilter: helpers.checkImageExtensionMulter,
}).single('image');

/**
 * List hotels
 */
module.exports.list = (req, res, next) => {
    const pagination = httpHelpers.getPagination(req);
    const filters = httpHelpers.getFilters(req, 'name', 'stars');
    HotelRepository.list(pagination, filters).then(result => {
        res.append(X_TOTAL_COUNT_HEADER_NAME, result.count);
        res.json(result.records);
    }).catch(error => {
        next(error);
    });
};

/**
 * Creates hotel
 */
module.exports.create = (req, res, next) => {
    const {error, value} = joi.validate(req.body, HotelDto, {
        abortEarly: false,
        context: {
            create: true
        }
    });
    if (error) {
        res.status(400).send(helpers.normalizeJoiError(error));
    } else {
        HotelRepository.create(value).then(result => {
            res.status(201).json(result);
        }).catch(error => {
            next(error);
        });
    }
};

/**
 * Uploads a hotel image
 */
module.exports.uploadImage = (req, res, next) => {
    const id = req.params.id;
    HotelRepository.find(id).then(result => {
        upload(result.id)(req, res, error => {
            if (error) {
                return next(error);
            }
            let server = process.env.BASE_SERVER || '';
            server = server.endsWith('/') ? server : `${server}/`;
            HotelRepository.updateImage(id, `${server}images/${req.file.filename}`).then(result => {
                res.json(result);
            }).catch(error => {
                console.error(error);
                next(error);
            });
        });
    }).catch(error => {
        next(error);
    })
};

/**
 * Find hotel
 */
module.exports.find = (req, res, next) => {
    const id = req.params.id;
    HotelRepository.find(id).then(result => {
        res.json(result);
    }).catch(error => {
        next(error);
    });
};

/**
 * Updates hotel
 */
module.exports.update = (req, res, next) => {
    const {error, value} = joi.validate(req.body, HotelDto, {
        abortEarly: false
    });
    if (error) {
        res.status(400).send(helpers.normalizeJoiError(error));
    } else {
        const id = req.params.id;
        HotelRepository.update(id, value).then(result => {
            res.json(result);
        }).catch(error => {
            next(error);
        });
    }
};

/**
 * Deletes hotel
 */
module.exports.delete = (req, res, next) => {
    const id = req.params.id;
    HotelRepository.delete(id).then(() => {
        res.status(204).json();
    }).catch(error => {
        next(error);
    });
};