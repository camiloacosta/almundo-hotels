import {Component} from '@angular/core';

@Component({
  selector: 'alm-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.scss']
})
export class BoxComponent {

  elevation = 1;
}
