export const environment = {
  production: true,
  server: 'http://localhost:9000',
  endpoint: 'http://localhost:9000/api'
};
