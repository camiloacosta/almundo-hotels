import {Component, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {ExpandAnimation} from '../../shared/animations/expand';

@Component({
  selector: 'alm-hotel-filter-stars',
  templateUrl: './hotel-filter-stars.component.html',
  styleUrls: ['./hotel-filter-stars.component.scss'],
  animations: [
    ExpandAnimation('expand')
  ]
})
export class HotelFilterStarsComponent implements OnInit {

  private readonly maxStars = 5;

  @Input()
  expanded;

  stars = [];

  selectedStars = [];

  all = true;

  @Output()
  selectStar = new Subject();

  ngOnInit(): void {
    this.stars = new Array(this.maxStars)
      .fill(0)
      .map((_, index) => this.maxStars - index - 1);
    this.selectedStars = new Array(this.maxStars).fill(false);
  }

  selectAll() {
    this.all = true;
    for (const star in this.selectedStars) {
      if (!this.selectedStars[star]) {
        continue;
      }
      this.selectedStars[star] = false;
    }
    this.emitSelectedStars();
  }

  selectOne(event, star) {
    this.selectedStars[star] = !this.selectedStars[star];
    this.all = !this.selectedStars.find(currentStar => currentStar);
    this.emitSelectedStars();
  }

  private emitSelectedStars() {
    const stars = this.selectedStars.reduce((stack, value, index) => {
      if (value) {
        stack.push(index);
      }
      return stack;
    }, []);
    this.selectStar.next(stars.map(star => star + 1));
  }

}
