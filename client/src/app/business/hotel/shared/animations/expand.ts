import {animate, state, style, transition, trigger} from '@angular/animations';

export function ExpandAnimation(triggerName) {
  return trigger(triggerName, [
    state('true', style({
      height: '*'
    })),
    state('false', style({
      height: '0px',
      display: 'none'
    })),
    transition('* => *', animate('300ms ease'))
  ]);
}
