import {Component, Output} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'alm-hotel-filter',
  templateUrl: './hotel-filter.component.html',
  styleUrls: ['./hotel-filter.component.scss', '../../shared/styles/expandable.scss']
})
export class HotelFilterComponent {

  private search = '';

  private stars = [];

  @Output()
  filter = new Subject();

  setSearch(search) {
    if (this.search === (search || '')) {
      return;
    }
    this.search = search;
    this.emitFilters();
  }

  setFilteredStars(stars) {
    this.stars = stars;
    this.emitFilters();
  }

  emitFilters() {
    this.filter.next({
      name: this.search,
      stars: this.stars.join(',')
    });
  }
}
