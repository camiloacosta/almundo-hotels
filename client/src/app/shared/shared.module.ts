import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BoxComponent} from './components/box/box.component';
import {ElevationDirective} from './directives/elevation.directive';
import { IconComponent } from './components/icon/icon.component';

@NgModule({
  declarations: [
    BoxComponent,
    ElevationDirective,
    IconComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    BoxComponent,
    ElevationDirective,
    IconComponent
  ]
})
export class SharedModule {
}
