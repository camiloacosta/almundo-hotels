import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {HotelRoutingModule} from './hotel-routing.module';
import {HotelListComponent} from './hotel-list/hotel-list.component';
import {SharedModule} from '../../shared/shared.module';
import {HotelFilterComponent} from './components/hotel-filter/hotel-filter.component';
import {HotelFilterSectionComponent} from './shared/components/hotel-filter-section/hotel-filter-section.component';
import {HotelFilterSearchComponent} from './components/hotel-filter-search/hotel-filter-search.component';
import {HotelFilterStarsComponent} from './components/hotel-filter-stars/hotel-filter-stars.component';
import {HotelListItemComponent} from './components/hotel-list-item/hotel-list-item.component';
import {CrudModule} from '../../crud/crud.module';
import {HotelService} from './hotel.service';
import {ExpandDirective} from './shared/directives/expand.directive';
import { HotelNoElementsComponent } from './components/hotel-no-elements/hotel-no-elements.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HotelRoutingModule,
    SharedModule,
    CrudModule
  ],
  declarations: [
    HotelListComponent,
    HotelFilterComponent,
    HotelFilterSectionComponent,
    HotelFilterSearchComponent,
    HotelFilterStarsComponent,
    HotelListItemComponent,
    ExpandDirective,
    HotelNoElementsComponent
  ],
  providers: [
    HotelService
  ]
})
export class HotelModule {
}
