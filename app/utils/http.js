const HttpError = require('../exceptions/http-error');

const MIN_LIMIT = 5;
const MAX_LIMIT = 100;
const INVALID_LIMIT = 'Limit param isn\'t valid';
const INVALID_PAGE = 'Page param isn\'t valid';

/**
 * Get pagination from query
 * @param req
 * @returns {{offset: number, limit: number}}
 */
module.exports.getPagination = req => {
    const pagination = {
        offset: null,
        limit: null
    };

    const all = req.query.all === 'true';
    if (all) {
        return pagination;
    }
    const page = req.query.p || req.query.page || 1;
    const limit = req.query.l || req.query.limit || MIN_LIMIT;
    if (isNaN(limit) || Number(limit) % 1 !== 0 || MIN_LIMIT > limit || limit > MAX_LIMIT) {
        console.log(100)
        throw new HttpError(400, INVALID_LIMIT);
    }
    if (isNaN(page) || Number(page) % 1 !== 0 || page < 1) {
        throw new HttpError(400, INVALID_PAGE);
    }
    pagination.offset = (parseInt(page) - 1) * parseInt(limit);
    pagination.limit = parseInt(limit);
    return pagination;
};

/**
 * Get specified filters from query
 * @param req
 * @param filters
 * @returns {{[p: string]: string}}
 */
module.exports.getFilters = (req, ...filters) => {
    const response = {};
    filters.forEach(filter => {
        if (!response.hasOwnProperty(filter) && req.query[filter]) {
            response[filter] = req.query[filter];
        }
    });
    return response;
};