import {CrudUrl} from './crud-url';
import {CrudService} from './services/crud.service';

export class CrudManager {

  dirty = false;

  items = [];

  totalItems = 0;

  page: number;

  limit: number;

  queryParams = {};

  constructor(private crudService: CrudService, url: Partial<CrudUrl>) {
    this.crudService.crudUrl = new CrudUrl(url);
    this.page = this.crudService.crudUrl.page;
    this.limit = this.crudService.crudUrl.limit;
    this.crudService.items$.subscribe(items => {
      this.items = items;
      this.totalItems = this.crudService.totalItems;
    });
  }

  updateList() {
    this.removeEmptyQueryParams();
    return this.crudService.list(this.queryParams).then(result => {
      this.dirty = true;
      return result;
    });
  }

  nextPage() {
    this.crudService.crudUrl.page++;
    this.page = this.crudService.crudUrl.page;
    return this.updateList();
  }

  previousPage() {
    this.crudService.crudUrl.page--;
    this.page = this.crudService.crudUrl.page;
    return this.updateList();

  }

  setLimit(limit: number) {
    this.crudService.crudUrl.limit = limit;
    this.limit = this.crudService.crudUrl.limit;
    this.resetPagination();
    return this.updateList();
  }

  setQueryParams(params: { [p: string]: any }) {
    for (const param in params) {
      if (!params.hasOwnProperty(param)) {
        continue;
      }
      this.queryParams[param] = params[param];
    }
    this.resetLimit();
    this.resetPagination();
  }

  removeEmptyQueryParams() {
    for (const param in this.queryParams) {
      if (!this.queryParams.hasOwnProperty(param)) {
        continue;
      }
      if (!this.queryParams[param]) {
        delete this.queryParams[param];
      }
    }
  }

  resetPagination() {
    this.crudService.crudUrl.page = 1;
    this.page = this.crudService.crudUrl.page;
  }

  resetLimit() {
    this.crudService.crudUrl.limit = 5;
    this.limit = this.crudService.crudUrl.limit;
  }
}
