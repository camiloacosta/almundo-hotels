import {Component, Input} from '@angular/core';
import {environment} from '../../../../../environments/environment';

const amenities = {
  'bathrobes': 'Batas de baño',
  'coffee-maker': 'Cafetera',
  'nightclub': 'Club nocturno',
  'bathtub': 'Bañera',
  'deep-soaking-bathtub': 'Bañera profunda',
  'restaurant': 'Restaurante',
  'beach-pool-facilities': 'Servicio de piscina',
  'fitness-center': 'Gimnasio',
  'safety-box': 'Caja de seguridad',
  'beach': 'Playa',
  'garden': 'Jardín',
  'separate-bedroom': 'Dormitorio separado',
  'business-center': 'Centro de negocios',
  'kitchen-facilities': 'Servicio de cocina',
  'sheets': 'Sábanas',
  'children-club': 'Club de niños',
  'newspaper': 'Periódico',
};

@Component({
  selector: 'alm-hotel-list-item',
  templateUrl: './hotel-list-item.component.html',
  styleUrls: ['./hotel-list-item.component.scss', '../../shared/styles/title.scss']
})
export class HotelListItemComponent {

  data: { name: string, image: string, stars: number, amenities: string[], price: number };

  amenities;

  @Input()
  set item(item) {
    this.data = item;
    // amenities
    const base = environment.server.endsWith('/') ? environment.server : `${environment.server}/`;
    this.amenities = this.data.amenities.map(amenity => ({
      url: `${base}assets/amenities/${amenity}.svg`,
      title: amenities[amenity]
    }));
  }

}
