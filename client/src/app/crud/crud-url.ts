export class CrudUrl {

  constructor(data: Partial<CrudUrl>) {
    this.url = data.url || '';
    this.page = data.page || 1;
    this.limit = data.limit || 10;
  }

  url: string;

  page: number;

  limit: number;
}
