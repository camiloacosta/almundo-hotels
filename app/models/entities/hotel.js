var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const HotelSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    stars: {
        type: Number,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    image: String,
    amenities: {
        type: [String],
        required: true,
    },
    createdAt: {
        type: Date,
        required: true,
    },
    updatedAt: Date
});

module.exports = mongoose.model('Hotel', HotelSchema);