const app = require('./app/app');
const http = require('http');
const server = http.createServer(app);
const mongoose = require('mongoose');

const port = process.env.PORT || 3000;

mongoose.connect(process.env.CONNECTION_STRING).catch(err => {
    console.error(err);
    app.close();
    process.exit(1);
});

mongoose.connection.once('open', () => {
    console.log('Database connection ready');

    server.listen(parseInt(port, 10), () => {
        console.log('Listen on', port, 'port');
    });
});


process.on('SIGTERM', () => {
    server.close();
    mongoose.connection.close();
    console.log('closing app');
});