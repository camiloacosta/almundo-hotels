import {CrudManager} from '../crud-manager';
import {Input} from '@angular/core';

export class PaginationController {

  @Input()
  crudManager: CrudManager;

  get canNext() {
    return this.crudManager.page < Math.ceil(this.crudManager.totalItems / this.crudManager.limit);
  }

  get canPrevious() {
    return this.crudManager.page > 1;
  }

  nextPage() {
    this.validate();
    this.canNext && this.crudManager.nextPage();
  }

  previousPage() {
    this.validate();
    this.canPrevious && this.crudManager.previousPage();
  }

  setLimit(limit: number) {
    this.validate();
    this.crudManager.setLimit(limit);
  }

  private validate() {
    if (!this.crudManager) {
      throw new Error('PaginationController -> validate: crudManager property is not defined');
    }
  }

}
