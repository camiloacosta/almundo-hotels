import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[almExpand]',
  exportAs: 'almExpand'
})
export class ExpandDirective {

  expanded = false;

  @HostListener('click') onClick() {
    this.almExpand = !this.expanded;
  }

  @Input()
  set almExpand(expanded: boolean) {
    if (expanded) {
      this.el.nativeElement.classList.add('expanded');
    } else {
      this.el.nativeElement.classList.remove('expanded');
    }
    this.expanded = expanded;
  }

  constructor(private el: ElementRef) {
    this.el.nativeElement.classList.add('expandable');
  }

}
