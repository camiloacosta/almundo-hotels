import {Injectable} from '@angular/core';
import {CrudManager} from '../../crud/crud-manager';
import {CrudService} from '../../crud/services/crud.service';
import {urls} from '../urls';

@Injectable({
  providedIn: 'root'
})
export class HotelService extends CrudManager {

  constructor(crudService: CrudService) {
    super(crudService, {
      url: urls.hotel
    });
  }
}
