import {Component, Input} from '@angular/core';

@Component({
  selector: 'alm-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent {

  @Input()
  icon: string;

  @Input()
  set repeat(value: number) {
    this.icons = new Array(value).fill(0);
  }

  icons = [0];
}
