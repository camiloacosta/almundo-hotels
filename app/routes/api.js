const express = require('express');
const router = express.Router();

const HotelController = require('../controllers/hotel.controller');

router.get('/', (req, res) => {
    res.end('Almundo Hotels')
});

router.route('/hotels')
    .get(HotelController.list) // List
    .post(HotelController.create); // Create

router.route('/hotels/:id')
    .get(HotelController.find) // Find
    .patch(HotelController.update) // Update
    .delete(HotelController.delete); // Delete

router.patch('/hotels/:id/image', HotelController.uploadImage);

module.exports = router;