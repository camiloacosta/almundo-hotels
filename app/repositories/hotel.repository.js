const path = require('path');
const fs = require('fs');
const HttpError = require('../exceptions/http-error');
const Hotel = require('../models/entities/hotel');
const {SOMETHING_HAPPENS, NOT_FOUND} = require('../utils/constants');

const HOTEL_ID_ALREADY_EXISTS = 'Hotel ID already exists';
const STARS_QUERY_PARAM_IS_INVALID = 'Query parameter `stars` is invalid';

/**
 * List hotels
 */
module.exports.list = (pagination, filters) => {
    filters.name = new RegExp(filters.name || '', 'i');
    if (filters.hasOwnProperty('stars')) {
        const stars = filters.stars.split(',');
        stars.forEach(star => {
            if (isNaN(star)) {
                throw new HttpError(400, STARS_QUERY_PARAM_IS_INVALID);
            }
        });
        filters.stars = {$in: stars.filter(star => star)};
    }
    return Hotel.count(filters)
        .then(count => {
            return Hotel.find(filters, null, {skip: pagination.offset, limit: pagination.limit})
                .then(records => {
                    records = records.map(result => ({
                        id: result._id,
                        name: result.name,
                        stars: result.stars,
                        price: result.price,
                        image: result.image,
                        amenities: result.amenities,
                        createdAt: result.createdAt,
                        updatedAt: result.updatedAt || null
                    }));

                    return {records, count};
                })
        })
        .catch(error => {
            console.error('HotelRepository -> list', error);
            throw new HttpError(500, SOMETHING_HAPPENS);
        });
};

/**
 * Creates hotel
 */
module.exports.create = (form) => {
    const hotel = new Hotel();
    hotel.name = form.name;
    hotel.stars = form.stars;
    hotel.price = form.price;
    hotel.image = form.image;
    hotel.amenities = form.amenities;
    hotel.createdAt = new Date();

    return hotel.save().then(result => {
        return {
            id: result._id,
            createdAt: result.createdAt
        };
    }).catch(error => {
        console.error('HotelRepository -> create', error);
        throw new HttpError(500, SOMETHING_HAPPENS);
    })
};

/**
 * Update hotel file path and updatedAt
 */
module.exports.updateImage = (id, image) => {
    const updatedAt = new Date();
    return Hotel.findByIdAndUpdate(id, {image, updatedAt}).then(result => {
        return {
            id: result._id,
            image: image,
            updatedAt: updatedAt
        };
    }).catch(error => {
        console.error(error);
        throw new HttpError(500, error);
    })
};

/**
 * Find hotel
 */
module.exports.find = (id) => {
    return Hotel.findById(id).then(result => {
        if (!result) {
            throw new HttpError(404, NOT_FOUND);
        }
        return {
            id: result._id,
            name: result.name,
            stars: result.stars,
            price: result.price,
            image: result.image,
            amenities: result.amenities,
            createdAt: result.createdAt,
            updatedAt: result.updatedAt || null
        };
    }).catch(error => {
        console.error('HotelRepository -> find', error);
        if (error instanceof HttpError) {
            throw error;
        }
        throw new HttpError(500, SOMETHING_HAPPENS);
    });
};

/**
 * Updates hotel
 */
module.exports.update = (id, form) => {
    return Hotel.findById(id).then(result => {
        if (!result) {
            throw new HttpError(404, NOT_FOUND);
        }
        return result;
    }).then(hotel => {
        for (field in form) {
            if (!hotel[field]) {
                continue;
            }
            hotel[field] = form[field];
        }
        hotel.updatedAt = new Date();
        return hotel.save().then(result => {
            return {
                id: result._id,
                name: result.name,
                stars: result.stars,
                price: result.price,
                image: result.image,
                amenities: result.amenities,
                createdAt: result.createdAt,
                updatedAt: result.updatedAt || null
            };
        }).catch(error => {
            console.error('HotelRepository -> update', error);
            throw error;
        })
    }).catch(error => {
        console.error('HotelRepository -> update', error);
        if (error instanceof HttpError) {
            throw error;
        }
        throw new HttpError(500, SOMETHING_HAPPENS);
    });
};

/**
 * Deletes hotel
 */
module.exports.delete = (id) => {
    return Hotel.findByIdAndDelete(id).then(result => {
        if (!result) {
            throw new HttpError(404, NOT_FOUND);
        }
        if (result.image) {
            const error = fs.unlinkSync(path.join(process.cwd(), result.image));
            if (error) {
                throw error;
            }
        }
    }).catch(error => {
        if (error instanceof HttpError) {
            throw error;
        } else {
            console.error('HotelRepository -> delete', error);
        }
        throw new HttpError(500, SOMETHING_HAPPENS);
    });
};