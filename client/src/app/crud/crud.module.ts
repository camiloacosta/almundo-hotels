import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CrudService} from './services/crud.service';
import {HttpClientModule} from '@angular/common/http';
import {PaginationComponent} from './components/pagination/pagination.component';

@NgModule({
  declarations: [
    PaginationComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    PaginationComponent
  ],
  providers: [
    CrudService
  ]
})
export class CrudModule {
}
