import {Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[almElevation]'
})
export class ElevationDirective {

  private previousElevation: number = null;

  @Input()
  set almElevation(elevation: number) {
    this.previousElevation && this.el.nativeElement.classList.remove(`elevation-${this.previousElevation}`);
    this.el.nativeElement.classList.add(`elevation-${elevation}`);
    this.previousElevation = elevation;
  }

  constructor(private el: ElementRef) {
  }

}
