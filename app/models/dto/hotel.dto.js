const joi = require('joi');

const amenities = ['bathrobes', 'coffee-maker', 'nightclub',
    'bathtub', 'deep-soaking-bathtub', 'restaurant',
    'beach-pool-facilities', 'fitness-center', 'safety-box',
    'beach', 'garden', 'separate-bedroom',
    'business-center', 'kitchen-facilities', 'sheets',
    'children-club', 'newspaper'];

module.exports = {
    name: joi.any().when('$create', {
        is: joi.boolean().valid(true).required(),
        then: joi.string().required(),
        otherwise: joi.string().optional()
    }),
    stars: joi.any().when('$create', {
        is: joi.boolean().valid(true).required(),
        then: joi.number().min(1).max(5).required(),
        otherwise: joi.number().min(1).max(5).optional()
    }),
    price: joi.any().when('$create', {
        is: joi.boolean().valid(true).required(),
        then: joi.number().min(0).required(),
        otherwise: joi.number().min(0).optional()
    }),
    amenities: joi.any().when('$create', {
        is: joi.boolean().valid(true).required(),
        then: joi.array().required().items(joi.string().valid(amenities)),
        otherwise: joi.array().optional().items(joi.string().valid(amenities))
    })
};