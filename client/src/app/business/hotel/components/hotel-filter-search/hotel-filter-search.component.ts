import {Component, Input, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {ExpandAnimation} from '../../shared/animations/expand';

@Component({
  selector: 'alm-hotel-filter-search',
  templateUrl: './hotel-filter-search.component.html',
  styleUrls: ['./hotel-filter-search.component.scss'],
  animations: [
    ExpandAnimation('expand')
  ]
})
export class HotelFilterSearchComponent {

  word: string;

  @Input()
  expanded;

  @Output()
  search = new Subject();
}
