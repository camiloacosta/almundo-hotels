import {CrudManager} from '../crud-manager';
import {OnInit} from '@angular/core';

export abstract class ListController implements OnInit {

  constructor(public crudManager: CrudManager) {
  }

  ngOnInit(): void {
    this.crudManager.updateList();
  }

  setFilter(filter) {
    this.crudManager.setQueryParams(filter);
    this.crudManager.updateList();
  }
}
