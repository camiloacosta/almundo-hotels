const mongoose = require('mongoose');
const Hotel = require('../app/models/entities/hotel');

mongoose.connect(process.env.CONNECTION_STRING).catch(err => {
    console.error(err);
    process.exit(1);
});

mongoose.connection.once('open', () => {
    console.log('Database connection ready');
    migrate();
});

const migrate = () => {

    const data = require('./data');

    let server = process.env.BASE_SERVER || '';
    server = server.endsWith('/') ? server : `${server}/`;

    data.map(item => {
        delete item.id;
        item.image = `${server}images/${item.image}`;
        item.createdAt = new Date();
    });

    console.log('Running migration');
    Hotel.create(data).then(() => {
        console.log('Migration ended');
        close();
    }).catch(error => {
        console.log(error);
        close();
    });

};

const close = () => {
    mongoose.connection.close();
    process.exit(0);
}