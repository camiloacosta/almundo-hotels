const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helpers = require('./utils/helpers');
const HttpError = require('./exceptions/http-error');
const {X_TOTAL_COUNT_HEADER_NAME} = require('./utils/constants');

const app = express();

const corsOptions = {
    origin: (process.env.CORS_WHITELIST).split(',') || [],
    exposedHeaders: [X_TOTAL_COUNT_HEADER_NAME]
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/images', express.static('images'));
app.use('/assets', express.static('assets'));

// Routes
const api = require('./routes/api');
app.use('/api', api);

app.use((error, req, res, next) => {
    if (error instanceof HttpError) {
        res.status(error.code).json(error.message);
    } else {
        next(error);
    }
});

if (helpers.isProduction()) {
    app.use('/', express.static('client/dist/client'));
}

module.exports = app;
