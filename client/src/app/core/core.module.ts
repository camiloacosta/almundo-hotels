import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CoreRoutingModule} from './core-routing.module';
import {MainComponent} from './bootstrap/main/main.component';
import {MainHeaderComponent} from './bootstrap/main-header/main-header.component';

@NgModule({
  declarations: [
    MainComponent,
    MainHeaderComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule
  ],
  exports: [
    MainComponent
  ]
})
export class CoreModule {
}
