import {Component} from '@angular/core';
import {PaginationController} from '../../controllers/pagination-controller';

@Component({
  selector: 'alm-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent extends PaginationController {

  limits = [5, 10, 20];
}
